use anyhow::Result;
use futures::prelude::*;
use regex::Regex;
use std::fs;
use std::sync::Mutex;
use std::{path::PathBuf, sync::Arc};
use tsclientlib::OutCommandExt;
use tsclientlib::{
    sync::{SyncConnection, SyncConnectionHandle},
    Connection, DisconnectOptions, Identity, Reason, StreamItem,
};
use tsproto_types::crypto::{EccKeyPrivP256, Error};

pub struct TeamSpeak {
    server_address: String,
    server_password: String,
    server_port: String,
    nickname: String,
    default_channel: String,
    channel_password: String,
    config_dir: PathBuf,
    key_file: PathBuf,
    connection_handle: Arc<Mutex<Option<SyncConnectionHandle>>>,
}

impl TeamSpeak {
    pub fn new(
        server_address: String,
        server_password: String,
        server_port: String,
        nickname: String,
        default_channel: String,
        channel_password: String,
        config_dir: PathBuf,
        key_file: PathBuf,
    ) -> Self {
        let port_regex = Regex::new(r"^[0-9]+$").unwrap();
        let teamspeak = TeamSpeak {
            server_address,
            server_password: if !server_password.is_empty() {
                server_password
            } else {
                String::new()
            },
            server_port: if !server_port.is_empty() && port_regex.is_match(server_port.as_str()) {
                server_port
            } else {
                String::from("9987")
            },
            nickname: if !nickname.is_empty() {
                nickname
            } else {
                String::from("TeamSpeakUser")
            },
            default_channel: if !default_channel.is_empty() {
                default_channel
            } else {
                String::new()
            },
            channel_password: if !channel_password.is_empty() {
                channel_password
            } else {
                String::new()
            },
            config_dir,
            key_file,
            connection_handle: Arc::new(Mutex::new(None)),
        };
        teamspeak
    }

    pub fn get_handle(&mut self) -> Arc<Mutex<Option<SyncConnectionHandle>>> {
        self.connection_handle.clone()
    }

    pub async fn connect(&mut self) -> Result<()> {
        //get the private key
        let private_key = get_private_key(self.config_dir.clone(), self.key_file.clone());
        //create identity with key
        let identity = Identity::new(private_key?, 0);

        //Prepare ip for connection
        let mut ip = self.server_address.clone();
        ip.push_str(format!(":{}", self.server_port.clone()).as_str());
        //Build a connection configuration
        let mut connection_config = Connection::build(ip).identity(identity).log_commands(true);
        if !self.server_password.is_empty() {
            connection_config = connection_config.password(self.server_password.clone());
        }
        if !self.nickname.is_empty() {
            connection_config = connection_config.name(self.nickname.clone());
        }
        if !self.default_channel.is_empty() {
            connection_config = connection_config.channel(self.default_channel.clone());
            if !self.channel_password.is_empty() {
                connection_config =
                    connection_config.channel_password(self.channel_password.clone());
            }
        }

        //Try creating connection to server
        let mut con = connection_config.connect()?;
        let result = con
            .events()
            .try_filter(|e| future::ready(matches!(e, StreamItem::BookEvents(_))))
            .next()
            .await;
        if let Some(result) = result {
            result?;
        }
        let connection: SyncConnection = con.into();
        let mut connection_handle = connection.get_handle();
        tokio::spawn(connection.for_each(|_| future::ready(())));
        // Wait until connected
        connection_handle.clone().wait_until_connected().await?;
        println!("Connected!");

        connection_handle
            .with_connection(move |connection_handle| {
                let state = connection_handle.get_state()?;
                //do stuff here
                println!("Server welcome message: {}", state.server.welcome_message);

                //send message
                //state.server.send_textmessage("Hello there").send(&mut connection)?;
                //state.send_message(tsclientlib::MessageTarget::Channel, "Hello").into_packet();
                Result::<_>::Ok(())
            })
            .await??;
        *self.connection_handle.lock().unwrap() = Some(connection_handle);
        Ok(())
    }

    pub async fn set_subscribed(&self, should_subscribe: Option<bool>) -> Result<()> {
        let handle = self.connection_handle.clone();
        handle
            .lock()
            .unwrap()
            .as_ref()
            .unwrap()
            .clone()
            .with_connection(move |con| {
                let state = con.get_state().unwrap();
                match should_subscribe {
                    Some(s) => {
                        state.server.set_subscribed(s).0;
                    }
                    _ => {
                        state.server.set_subscribed(true).0;
                    }
                }
            })
            .await
            .ok();
        Ok(())
    }

    pub async fn send_message(&self, message: String) -> Result<()> {
        let mut handle = self
            .connection_handle
            .clone()
            .lock()
            .unwrap()
            .as_ref()
            .unwrap()
            .clone();
        handle
            .with_connection(move |connection_handle| {
                let state = connection_handle.get_state()?;
                state.server.set_subscribed(true).into_packet();
                state
                    .send_message(tsclientlib::MessageTarget::Channel, message.as_str())
                    .send(connection_handle)?;
                Result::<_>::Ok(())
            })
            .await??;
        Ok(())
    }

    pub async fn disconnect(&self) -> Result<()> {
        // Disconnect
        let options = DisconnectOptions::new()
            .reason(Reason::Clientdisconnect)
            .message("leaving");
        let handle = self.connection_handle.clone();
        if handle.lock().unwrap().is_some() {
            let mut con_handle = handle.lock().unwrap().as_ref().unwrap().clone();
            con_handle.disconnect(options).await?;
        }
        println!("Disconnected!");
        Ok(())
    }
}

fn get_private_key(config_dir: PathBuf, key_file: PathBuf) -> Result<EccKeyPrivP256, Error> {
    let mut key_path: PathBuf = config_dir.join(&key_file);
    if key_file.is_absolute() {
        key_path.push(&key_file)
    }
    let private_key = match fs::read(&key_path) {
        //Use existing key
        Ok(key) => EccKeyPrivP256::import(&key)?,

        //Create new key
        _ => {
            let key = EccKeyPrivP256::create();
            //Create config dir for key
            if let Err(e) = fs::create_dir_all(&config_dir) {
                println!("Error! Failed to create config dir! error: {:?}", e)
            }
            //Write new key to a file
            if let Err(e) = fs::write(&key_path, &key.to_short()) {
                eprintln!(
                    "Warning! Failed to create the private key file! This means \
                that the key will have to be created again on the next run.\nError: {}",
                    e
                )
            }

            key
        }
    };
    Ok(private_key)
}
